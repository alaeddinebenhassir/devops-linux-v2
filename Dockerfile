FROM node:20-alpine


# Create app directory
WORKDIR /home/app

COPY package*.json ./

RUN npm update

# we start with just installing the dependencies in a seprate layer , in order to optimize later updates

# Building the app vvvv
COPY . .
RUN cd client/ && npm update && cd ..
RUN npm run build

EXPOSE 8080

CMD [ "node", "app" ]
